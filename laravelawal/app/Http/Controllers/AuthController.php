<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('form');
    }

    public function daftar(){
       $namadepan = request('name1');
       $namabelakang = request('name2');

       return view('welcome',['first'=>$namadepan,'last'=>$namabelakang]);
    }
}
