<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
    @csrf
        <label for="">First Name :</label><br><br>
        <input type="text" name="name1"><br><br>
        <label for="">Last Name :</label><br><br>
        <input type="text" name="name2" id=""><br><br>
        <label for="">Gender :</label><br><br>
        <input type="radio" name="gender" id="">Male<br>
        <input type="radio" name="gender" id="">Female<br>
        <input type="radio" name="gender" id="">Other<br><br>
        <label for="">Nationality :</label><br><br>
        <select name="nation" id="">
            <option value="indonesian">Indonesian</option>
            <option value="malaysian">Malaysian</option>
            <option value="singaporean">Singaporean</option>
            <option value="australian">Australian</option>
        </select><br><br>
        <label for="">Languange Spoken :</label><br><br>
        <input type="checkbox" name="" id="">Bahasa Indonesian<br>
        <input type="checkbox" name="" id="">English<br>
        <input type="checkbox" name="" id="">Other<br><br>
        <label for="">Bio :</label><br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
        <button>Sign Up</button>
    </form>
</body>
</html>