@extends('Layout.master')
@section('judul')
    Halaman Detail Cast Id {{$cast->id}}
@endsection

@section('isi')
<h2>Biodata dengan ID : {{$cast->id}}</h2>
<p>Nama :{{$cast->nama}}</p>
<p>Umur :{{$cast->umur}}</p>
<p>Bio  :{{$cast->bio}}</p>
@endsection